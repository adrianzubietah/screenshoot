const express = require('express');
const bodyParser = require('body-parser');
const screenShoot = require('./screenShoot/screenShot');

const port = 3000;
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.listen(port, () => {
  console.log('El servidor está inicializado en el puerto ' + port);
});

app.post('/screenShoot', async function (req, res){
  const response = await screenShoot.getBase64(req.body);
  res.send(response);
})



