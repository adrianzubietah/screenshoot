const puppeteer = require('puppeteer');

exports.getBase64 = async ({ url }) => {
    const response = {} 
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(url);
    const base64 = await page.screenshot({encoding: "base64", fullPage: true });
    await browser.close();
    response.base64 = base64;
    return response;
}

