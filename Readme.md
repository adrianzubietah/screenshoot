# Screen-Shot
Aplicacion Nods.js para obtener la base64 del screenShot de una pagina web dada.

## Request

```
{
	"url" : "{url}"
}
```

## Response


```
{
	"base64" : "{base64}"
}

```
